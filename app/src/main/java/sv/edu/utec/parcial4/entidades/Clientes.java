package sv.edu.utec.parcial4.entidades;

public class Clientes {

    private int ID_Cliente ;
    private String sNombreCliente;
    private String sApellidosCliente;
    private String sDireccionCliente;
    private String sCiudadCliente;


    public int getID_Cliente() {
        return ID_Cliente;
    }

    public void setID_Cliente(int ID_Cliente) {
        this.ID_Cliente = ID_Cliente;
    }

    public String getsNombreCliente() {
        return sNombreCliente;
    }

    public void setsNombreCliente(String sNombreCliente) {
        this.sNombreCliente = sNombreCliente;
    }

    public String getsApellidosCliente() {
        return sApellidosCliente;
    }

    public void setsApellidosCliente(String sApellidosCliente) {
        this.sApellidosCliente = sApellidosCliente;
    }

    public String getsDireccionCliente() {
        return sDireccionCliente;
    }

    public void setsDireccionCliente(String sDireccionCliente) {
        this.sDireccionCliente = sDireccionCliente;
    }

    public String getsCiudadCliente() {
        return sCiudadCliente;
    }

    public void setsCiudadCliente(String sCiudadCliente) {
        this.sCiudadCliente = sCiudadCliente;
    }
}
