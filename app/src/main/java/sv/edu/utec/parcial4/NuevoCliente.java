package sv.edu.utec.parcial4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sv.edu.utec.parcial4.db.DBClientes;

public class NuevoCliente extends AppCompatActivity {
    EditText txtNombre,txtDescripcion,txtApellido, txtCiudad;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_cliente);
        txtNombre=findViewById(R.id.edtNombre);
        txtApellido=findViewById(R.id.edtApellido);
        txtDescripcion=findViewById(R.id.edDescripcion);
        txtCiudad=findViewById(R.id.edtCiudad);
        btnSave=findViewById(R.id.btnGuardar);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long id=0;
                DBClientes dbClientes= new DBClientes(NuevoCliente.this);
                id=dbClientes.insertarArticulos(txtNombre.getText().toString(),
                        txtApellido.getText().toString(),txtDescripcion.getText().toString(),txtCiudad.getText().toString());

                if(id>0){
                    Toast.makeText(NuevoCliente.this, "Datos Insertados correctamente", Toast.LENGTH_LONG).show();
                    limpiarcampos();
                }
                else{
                    Toast.makeText(NuevoCliente.this, "Error en insertar Datos", Toast.LENGTH_LONG).show();
                }

            }
        });


    }
    private void limpiarcampos(){
        txtNombre.setText("");
        txtDescripcion.setText("");
        txtApellido.setText("");
        txtCiudad.setText("");
    }
}