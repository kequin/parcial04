package sv.edu.utec.parcial4.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import sv.edu.utec.parcial4.entidades.Clientes;

public class DBClientes extends  BDHelper{
    Context contexto;
    public DBClientes(@Nullable Context context) {
        super(context);
        this.contexto=context;
    }

    public long insertarArticulos(String nombre,String apellido,String descripcion, String ciudad){
        long id=0;
        try {
            BDHelper dbHelper = new BDHelper(contexto);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues valores = new ContentValues();
            valores.put("sNombreCliente", nombre);
            valores.put("sApellidosCliente", apellido);
            valores.put("sDireccionCliente", descripcion);
            valores.put("sCiudadCliente", ciudad);

            id= db.insert(TABLA_CLIENTES, null, valores);
        }
        catch (Exception ex){
            ex.toString();
        }
        return id;
    }


    public ArrayList<Clientes> mostrarContactos(){
        BDHelper dbHelper = new BDHelper(contexto);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ArrayList<Clientes>listaArticulos= new ArrayList<>();
        Clientes articulo;
        Cursor cursorClientes=null;

        cursorClientes=db.rawQuery("SELECT * FROM "+TABLA_CLIENTES,null);
        if(cursorClientes.moveToFirst()){
            do{
                articulo= new Clientes();
                articulo.setID_Cliente(cursorClientes.getInt(0));
                articulo.setsNombreCliente(cursorClientes.getString(1));
                articulo.setsApellidosCliente(cursorClientes.getString(2));
                articulo.setsDireccionCliente(cursorClientes.getString(3));
                articulo.setsCiudadCliente(cursorClientes.getString(4));
                listaArticulos.add(articulo);
            }while(cursorClientes.moveToNext());

        }
        cursorClientes.close();
        return listaArticulos;

    }

  /*  public Articulos verificarContactos(int cod){
        BDHelper dbHelper = new BDHelper(contexto);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Articulos articulo=null;
        Cursor cursorContactos=null;

        cursorContactos=db.rawQuery("SELECT * FROM "+TABLA_ARTICULOS+" WHERE id="+cod+" LIMIT 1",null);
        if(cursorContactos.moveToFirst()){

            articulo= new Articulos();
            articulo.setId(cursorContactos.getInt(0));
            articulo.setNombre(cursorContactos.getString(1));
            articulo.setTelefono(cursorContactos.getString(2));
            articulo.setCorreoelectronico(cursorContactos.getString(3));
            articulo.setInstagram(cursorContactos.getString(4));

        }

        cursorContactos.close();
        return articulo;

    }*/

    public boolean actualizarClientes(int id,String nombre,String apellido,String direccion, String ciudad){

        boolean estado=false;
        BDHelper dbHelper = new BDHelper(contexto);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {

            db.execSQL(" UPDATE "+TABLA_CLIENTES+ " SET sNombreCliente='"+nombre+"',"+
                    "sApellidosCliente='"+apellido+"',"+
                    "sDireccionCliente='"+direccion+"',"+
                    "sCiudadCliente='"+ciudad+"'" +
                    " WHERE id="+id);
            estado=true;
        }
        catch (Exception ex){
            ex.toString();
            estado=false;
        }finally {
            db.close();
        }
        return estado;
    }


    public boolean eliminarClientes(int id){

        boolean estado=false;
        BDHelper dbHelper = new BDHelper(contexto);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {

            db.execSQL("DELETE FROM " + TABLA_CLIENTES+
                    " WHERE id="+id+";");
            estado=true;
        }
        catch (Exception ex){
            ex.toString();
            estado=false;
        }finally {
            db.close();
        }
        return estado;
    }
}
