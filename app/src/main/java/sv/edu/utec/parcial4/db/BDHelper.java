package sv.edu.utec.parcial4.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;


public class BDHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NOMBRE="TiendaAutos.db";
    public static final String TABLA_CLIENTES="MD_CLIENTES";
    public static final String TABLA_CLIENTE_VEHICULO="MD_CLIENTE_VEHICULO";
    public static final String TABLA_MD_VEHICULOS="MD_VEHICULOS";

    public BDHelper(@Nullable Context context) {
        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase base) {
        base.execSQL("CREATE TABLE "+ TABLA_CLIENTES +"("+
                "ID_Cliente INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "sNombreCliente TEXT NOT NULL,"+
                "sApellidosCliente TEXT NOT NULL,"+
                "sDireccionCliente TEXT NOT NULL,"+
                "sCiudadCliente TEXT NOT NULL)");

        base.execSQL("CREATE TABLE "+ TABLA_CLIENTE_VEHICULO +"("+
                "ID_Cliente INTEGER,"+
                "ID_Vehiculo INTEGER,"+
                "sMatricula TEXT NOT NULL,"+
                "iKilometros TEXT NOT NULL)");

        base.execSQL("CREATE TABLE "+ TABLA_MD_VEHICULOS +"("+
                "ID_Vehiculo INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "sMarca TEXT NOT NULL,"+
                "sModelo TEXT NOT NULL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase base, int i, int i1) {
        base.execSQL("DROP TABLE " + TABLA_CLIENTES);
        base.execSQL("DROP TABLE " + TABLA_CLIENTE_VEHICULO);
        base.execSQL("DROP TABLE " + TABLA_MD_VEHICULOS);
        onCreate(base);
    }
}
