package sv.edu.utec.parcial4.adaptador;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sv.edu.utec.parcial4.R;
import sv.edu.utec.parcial4.entidades.Clientes;

public class ListaClienteAdapter extends RecyclerView.Adapter<ListaClienteAdapter.ClienteViewHolder>{

    ArrayList<Clientes> listaClientes;
    @NonNull
    @Override
    public ClienteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_items_clientes,null,false);
        return new ClienteViewHolder(view);
    }
    public ListaClienteAdapter(ArrayList<Clientes> listaClientes){
        this.listaClientes=listaClientes;
    }
    @Override
    public void onBindViewHolder(@NonNull ClienteViewHolder holder, int position) {
        holder.tviNombre.setText(listaClientes.get(position).getsNombreCliente());
        holder.tviApellido.setText(listaClientes.get(position).getsApellidosCliente());
        holder.tviDireccion.setText(listaClientes.get(position).getsDireccionCliente());
        holder.tviCiudad.setText(listaClientes.get(position).getsCiudadCliente());
    }

    @Override
    public int getItemCount() {
        return   listaClientes.size();
    }

    public class ClienteViewHolder extends RecyclerView.ViewHolder {
        TextView tviNombre, tviApellido,tviDireccion, tviCiudad;
        public ClienteViewHolder(@NonNull View itemView) {
            super(itemView);
            tviNombre=itemView.findViewById(R.id.tvNombre);
            tviApellido=itemView.findViewById(R.id.tvApellido);
            tviDireccion=itemView.findViewById(R.id.tvDireccion);
            tviCiudad=itemView.findViewById(R.id.tvCiudad);


        }
    }
}
