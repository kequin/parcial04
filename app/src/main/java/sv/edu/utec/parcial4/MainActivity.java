package sv.edu.utec.parcial4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

import sv.edu.utec.parcial4.adaptador.ListaClienteAdapter;
import sv.edu.utec.parcial4.db.DBClientes;
import sv.edu.utec.parcial4.entidades.Clientes;

public class MainActivity extends AppCompatActivity {
   // Button btnCrear;
    RecyclerView lstClientes;
    ArrayList<Clientes> lstArrayCliente;
    @Override
   protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lstClientes= findViewById(R.id.lstClientes);
        lstClientes.setLayoutManager(new LinearLayoutManager(this));
        DBClientes dbclientes=new DBClientes(MainActivity.this);
        lstArrayCliente= new ArrayList<>();

        ListaClienteAdapter adaptadores = new ListaClienteAdapter(dbclientes.mostrarContactos());
        lstClientes.setAdapter(adaptadores);

       // btnCrear=findViewById(R.id.btnCrear);

     /*   btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BDHelper DBH = new BDHelper(MainActivity.this);
                SQLiteDatabase database= DBH.getWritableDatabase();
            }
        });*/
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuprincipal,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menuNuevo:
                nuevoRegistro();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void nuevoRegistro(){
        Intent inte = new Intent(this, NuevoCliente.class);
        startActivity(inte);

    }
}